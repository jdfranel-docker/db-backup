#!/bin/sh

# shellcheck disable=SC1091
. backup_base

MYSQL_PORT=${MYSQL_PORT:=3306}
MYSQL_HOST=${MYSQL_HOST:=db}

if [ -z "$MYSQL_DATABASE" ]; then
  echo "Missing environment variable \$MYSQL_DATABASE"
  exit 31
fi

if [ -z "$MYSQL_USER" ]; then
  echo "Missing environment variable \$MYSQL_USER"
  exit 31
fi

if [ -z "$MYSQL_PASSWORD" ]; then
  echo "Missing environment variable \$MYSQL_PASSWORD"
  exit 31
fi

TABLESPACES="--no-tablespaces"
if [ -n "$MYSQL_DUMP_TABLESPACES" ]; then
  TABLESPACES=""
fi

BACKUP_FILE_EXT=".sql" # format: plain
# shellcheck disable=SC2034
BACKUP_FILE_NAME="$BACKUP_DATE-$MYSQL_DATABASE$BACKUP_FILE_EXT"

echo "Backing up database $MYSQL_DATABASE with user $MYSQL_USER on host $MYSQL_HOST:$MYSQL_PORT. Saving on $(backup_path)"
mysqldump $TABLESPACES -P $MYSQL_PORT -h $MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE > "$(backup_path)"

on_backup_success
