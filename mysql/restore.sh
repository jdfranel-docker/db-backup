#!/bin/sh

# shellcheck disable=SC1091
. restore_base

MYSQL_PORT=${MYSQL_PORT:=3306}
MYSQL_HOST=${MYSQL_HOST:=db}

if [ -z "$MYSQL_DATABASE" ]; then
  echo "Missing environment variable \$MYSQL_DATABASE"
  exit 31
fi

if [ -z "$MYSQL_USER" ]; then
  echo "Missing environment variable \$MYSQL_USER"
  exit 31
fi

if [ -z "$MYSQL_PASSWORD" ]; then
  echo "Missing environment variable \$MYSQL_PASSWORD"
  exit 31
fi

# shellcheck disable=SC2034
RESTORE_FILE_EXT=".sql"

while getopts "f:" option
do
  case $option in
    f)
      SELECTED_BACKUP=$OPTARG
      ;;
    *)
      ;;
  esac
done

init_file_to_restore "$SELECTED_BACKUP"
check_dump_exists
pre_process_backup

echo "Restoring dump $RESTORE_FILE_NAME into database $MYSQL_DATABASE with user $MYSQL_USER on host $MYSQL_HOST:$MYSQL_PORT"
mysql -P $MYSQL_PORT -h $MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_DATABASE < $RESTORE_FILE_NAME
echo "Restored $RESTORE_FILE_NAME"
