#!/bin/sh

# shellcheck disable=SC1091
. backup_base

POSTGRES_PORT=${POSTGRES_PORT:=5432}
POSTGRES_HOST=${POSTGRES_HOST:=db}
BACKUP_FORMAT=${POSTGRES_BACKUP_FORMAT:=custom}

if [ -z "$POSTGRES_DB" ]; then
  echo "Missing environment variable \$POSTGRES_DB"
  exit 32
fi

if [ -z "$POSTGRES_USER" ]; then
  echo "Missing environment variable \$POSTGRES_USER"
  exit 32
fi

if [ -z "$POSTGRES_PASSWORD" ]; then
  echo "Missing environment variable \$POSTGRES_PASSWORD"
  exit 32
fi

BACKUP_FILE_EXT=".dump" # format: custom
if [ "$BACKUP_FORMAT" = "tar" ]; then
  BACKUP_FILE_EXT=".tar"
elif [ "$BACKUP_FORMAT" = "plain" ]; then
  BACKUP_FILE_EXT=".sql"
elif [ "$BACKUP_FORMAT" = "directory" ]; then
  BACKUP_FILE_EXT=""
fi

# shellcheck disable=SC2034
BACKUP_FILE_NAME="$BACKUP_DATE-$POSTGRES_DB$BACKUP_FILE_EXT"

echo "Backing up database $POSTGRES_DB with user $POSTGRES_USER on host $POSTGRES_HOST:$POSTGRES_PORT. Saving on $(backup_path)"
pg_dump -f "$(backup_path)" -F $BACKUP_FORMAT --dbname="postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"

on_backup_success
