#!/bin/sh

# shellcheck disable=SC1091
. restore_base

POSTGRES_PORT=${POSTGRES_PORT:=5432}
POSTGRES_HOST=${POSTGRES_HOST:=db}
BACKUP_FORMAT=${POSTGRES_BACKUP_FORMAT:=custom}

if [ -z "$POSTGRES_DB" ]; then
  echo "Missing environment variable \$POSTGRES_DB"
  exit 32
fi

if [ -z "$POSTGRES_USER" ]; then
  echo "Missing environment variable \$POSTGRES_USER"
  exit 32
fi

if [ -z "$POSTGRES_PASSWORD" ]; then
  echo "Missing environment variable \$POSTGRES_PASSWORD"
  exit 32
fi

RESTORE_FILE_EXT=".dump"
if [ "$BACKUP_FORMAT" = "tar" ]; then
  RESTORE_FILE_EXT=".tar"
elif [ "$BACKUP_FORMAT" = "plain" ]; then
  RESTORE_FILE_EXT=".sql"
elif [ "$BACKUP_FORMAT" = "directory" ]; then
  # shellcheck disable=SC2034
  RESTORE_FILE_EXT=""
fi

while getopts "cf:" option
do
  case $option in
    c)
      OWNERSHIP="--no-owner --role=$POSTGRES_USER"
      ;;
    f)
      SELECTED_BACKUP=$OPTARG
      ;;
    *)
      ;;
  esac
done

init_file_to_restore "$SELECTED_BACKUP"
check_dump_exists
pre_process_backup

JOBS=1
if [ "$(nproc)" -gt 2 ]; then
  JOBS=$(($(nproc) - 1))
fi
JOBSOPTION=""
if [ "$BACKUP_FORMAT" = "custom" ]; then
  JOBSOPTION="--jobs=$JOBS"
fi

echo "Restoring dump $RESTORE_FILE_NAME into database $POSTGRES_DB with user $POSTGRES_USER on host $POSTGRES_HOST:$POSTGRES_PORT"
POSTGRES_CONNEXION="postgres://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/$POSTGRES_DB"
if [ "$BACKUP_FORMAT" = "plain" ]; then
  psql -c "DROP SCHEMA public CASCADE;" -c "CREATE SCHEMA public;" -c "GRANT ALL ON SCHEMA public TO $POSTGRES_USER;" -c "GRANT ALL ON SCHEMA public TO public;" "$POSTGRES_CONNEXION"
  psql -f "$RESTORE_FILE_NAME" "$POSTGRES_CONNEXION"
else
  # shellcheck disable=SC2086
  pg_restore $OWNERSHIP --clean $JOBSOPTION -F $BACKUP_FORMAT --dbname=$POSTGRES_CONNEXION $RESTORE_FILE_NAME
fi
echo "Restored $RESTORE_FILE_NAME"
