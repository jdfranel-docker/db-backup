#!/bin/sh

if [ -n "$TZ" ]; then
  cp /usr/share/zoneinfo/${TZ} /etc/localtime
  echo "${TZ}" > /etc/timezone
fi

if [ -n "$CRON_SCHEDULE" ]; then
  echo "Generating crontabs ${CRON_SCHEDULE}"
  echo "${CRON_SCHEDULE} backup" > /etc/crontabs/root
fi

exec "$@"