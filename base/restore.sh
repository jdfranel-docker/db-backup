#!/bin/sh
MAIN_RESTORE_FOLDER="/dbdump"
BACKUP_FORMAT=""
RESTORE_FILE_EXT=""
RESTORE_FILE_NAME=""
PRE_PROCESSED_RESTORE_FILE_NAME=""
COMPRESS=${COMPRESS:=0}
SKIP_UNCOMPRESS=0 # 1 will prevent post-processing to compress

backup_directory_format() {
  if [ "$BACKUP_FORMAT" = "directory" ]; then
    echo "1"
  fi
}

pre_processed_file_ext() {
  if [ -n "$(backup_directory_format)" ] && [ ! $COMPRESS = "1" ]; then
    echo ""
  else
    _PRE_PROCESSED_FILE_EXT=""
    if [ $COMPRESS = "1" ] && [ $SKIP_UNCOMPRESS = "0" ]; then
      if [ -n "$(backup_directory_format)" ]; then
        _PRE_PROCESSED_FILE_EXT=".tar"
      fi
      _PRE_PROCESSED_FILE_EXT="${_PRE_PROCESSED_FILE_EXT}.gz"
    fi
    echo "$_PRE_PROCESSED_FILE_EXT"
  fi
}

latest_backup() {
  if [ -n "$(backup_directory_format)" ] && [ ! $COMPRESS = "1" ]; then
    echo "$(ls -dtr $MAIN_RESTORE_FOLDER/*/ | tail -n 1)"
  else
    _LATEST_BACKUP_PRE_PROCESSED_RESTORE_FILE_EXT=$(pre_processed_file_ext)
    echo "$(ls -tr $MAIN_RESTORE_FOLDER/*$RESTORE_FILE_EXT$_LATEST_BACKUP_PRE_PROCESSED_RESTORE_FILE_EXT | tail -n 1)"
  fi
}

init_file_to_restore() {
  _INIT_FILE_TO_RESTORE_SELECTED_BACKUP=$1
  if [ -n "$_INIT_FILE_TO_RESTORE_SELECTED_BACKUP" ]; then
    PRE_PROCESSED_RESTORE_FILE_NAME="$MAIN_RESTORE_FOLDER/$_INIT_FILE_TO_RESTORE_SELECTED_BACKUP"
  else
    PRE_PROCESSED_RESTORE_FILE_NAME=$(latest_backup)
  fi
  RESTORE_FILE_NAME=$(echo "$PRE_PROCESSED_RESTORE_FILE_NAME" | sed -e "s/^\(.*\)$(pre_processed_file_ext)\$/\1/")
  if [ -z "$PRE_PROCESSED_RESTORE_FILE_NAME" ]; then
    echo "No dump to restore"
    exit 11
  fi
}

check_dump_exists() {
  if [ -n "$(backup_directory_format)" ] && [ ! $COMPRESS = "1" ]; then
    if ! [ -d "$PRE_PROCESSED_RESTORE_FILE_NAME" ]; then
      echo "Dump directory $PRE_PROCESSED_RESTORE_FILE_NAME do NOT exists"
      exit 12
    fi
  else
    if [ ! -f "$PRE_PROCESSED_RESTORE_FILE_NAME" ]; then
      echo "Dump $PRE_PROCESSED_RESTORE_FILE_NAME do NOT exists"
      exit 13
    fi
  fi
}

pre_process_backup() {
  if [ $COMPRESS = "1" ] && [ $SKIP_UNCOMPRESS = "0" ]; then
    if [ -n "$(backup_directory_format)" ]; then
      tar -xzf "$PRE_PROCESSED_RESTORE_FILE_NAME"
    else
      gzip -dk "$PRE_PROCESSED_RESTORE_FILE_NAME"
    fi
  fi
}

on_restore_exit() {
  if [ $COMPRESS = "1" ] && [ $SKIP_UNCOMPRESS = "0" ]; then
    if [ -f "$RESTORE_FILE_NAME" ] || [ -d "$RESTORE_FILE_NAME" ]; then
      rm -rf "$RESTORE_FILE_NAME"
    fi
  fi
}

set -e
trap on_restore_exit 0