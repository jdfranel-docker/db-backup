#!/bin/sh
MAIN_BACKUP_FOLDER="/dbdump"
BACKUP_DATE=$(date '+%Y-%m-%d_%H.%M.%S_%z')
BACKUP_FORMAT=""
BACKUP_FILE_EXT=".backup"
BACKUP_FILE_NAME="$BACKUP_DATE$BACKUP_FILE_EXT"
PID_FILE="/backup.pid"
COMPRESS=${COMPRESS:=0}
SKIP_COMPRESS=0 # 1 will prevent post-processing to compress

backup_path() {
  echo "$MAIN_BACKUP_FOLDER/$BACKUP_FILE_NAME"
}

backup_directory_format() {
  if [ "$BACKUP_FORMAT" = "directory" ]; then
    echo "1"
  fi
}

on_backup_success() {
  check_result
  post_process_backup
  set_ownership
  remove_lock
  apply_retain_policy
}

check_result() {
  if [ ! -e "$(backup_path)" ]; then
    echo "Backup failed"
    exit 20
  else
    if [ -d "$(backup_path)" ]; then
      _CHECK_RESULT_BACKUP_SIZE=$(du -h "$(backup_path)" | awk '{ print $1 }')
      if [ "$_CHECK_RESULT_BACKUP_SIZE" = 4 ]; then
        echo "Backup failed"
        rm -rf "$(backup_path)"
        exit 22
      fi
      echo "Generated backup directory size is ${_CHECK_RESULT_BACKUP_SIZE}"
    else
      _CHECK_RESULT_BACKUP_SIZE=$(du -h "$(backup_path)" | awk '{ print $1 }')
      if [ "$_CHECK_RESULT_BACKUP_SIZE" = 0 ]; then
        echo "Backup failed"
        rm -f "$(backup_path)"
        exit 21
      fi
      echo "Generated backup file size is ${_CHECK_RESULT_BACKUP_SIZE}"
    fi
  fi
}

post_process_backup() {
  if [ $COMPRESS = "1" ] && [ $SKIP_COMPRESS = "0" ]; then
    if [ -n "$(backup_directory_format)" ]; then
      tar -czf "$(backup_path).tar.gz" "$(backup_path)"
      rm -rf "$(backup_path)"
      BACKUP_FILE_NAME="$BACKUP_FILE_NAME.tar.gz"
      BACKUP_FILE_EXT=".tar.gz"
    else
      gzip "$(backup_path)"
      BACKUP_FILE_NAME="$BACKUP_FILE_NAME.gz"
      BACKUP_FILE_EXT="$BACKUP_FILE_EXT.gz"
    fi
    _POST_PROCESS_BACKUP_SIZE=$(du -h "$(backup_path)" | awk '{ print $1 }')
    echo "Compressed backup file size is ${_POST_PROCESS_BACKUP_SIZE}"
  fi
}

set_ownership() {
  if [ -n "$FILE_UID" ]; then
    _SET_OWNERSHIP_OWNER=$FILE_UID
    if [ -n "$FILE_GID" ]; then
      _SET_OWNERSHIP_OWNER="${_SET_OWNERSHIP_OWNER}:$FILE_GID"
    fi
    echo "Changing file owner for $_SET_OWNERSHIP_OWNER"
    chown -R "$_SET_OWNERSHIP_OWNER" "$(backup_path)"
  fi
  if [ -n "$FILE_CHMOD" ]; then
    echo "Changing file permission to $FILE_CHMOD"
    chmod -R "$FILE_CHMOD" "$(backup_path)"
  else
    echo "Changing file permission to defaut"
    if [ -d "$(backup_path)" ]; then
      chmod 0755 "$(backup_path)"
      find "$(backup_path)" -type d -print0 | xargs -0 chmod 0755
      find "$(backup_path)" -type f -print0 | xargs -0 chmod 0644
    else
      chmod 0644 "$(backup_path)"
    fi
  fi
}

apply_retain_policy() {
  _APPLY_RETAIN_POLICY_DIRECTORY_FORMAT=0
  if [ -n "$(backup_directory_format)" ] && [ ! $COMPRESS = "1" ]; then
    _APPLY_RETAIN_POLICY_DIRECTORY_FORMAT=1
  fi
  if [ -n "$RETAIN_COUNT" ] && [ "$(expr "$RETAIN_COUNT" : "[0-9]+$")" ] && [ "$RETAIN_COUNT" -gt 0 ]; then
    if [ $_APPLY_RETAIN_POLICY_DIRECTORY_FORMAT = 1 ]; then
      EXISTING_BACKUPS=$(ls -1d ${MAIN_BACKUP_FOLDER}/*/)
    else
      EXISTING_BACKUPS=$(ls -1 ${MAIN_BACKUP_FOLDER}/*$BACKUP_FILE_EXT)
    fi

    _APPLY_RETAIN_POLICY_BACKUP_COUNT=$(echo "${EXISTING_BACKUPS}"  | wc -l)
    echo "There is currently $_APPLY_RETAIN_POLICY_BACKUP_COUNT backups"

    _APPLY_RETAIN_POLICY_OBSOLETE_COUNT=$((_APPLY_RETAIN_POLICY_BACKUP_COUNT - RETAIN_COUNT))
    if [ "$_APPLY_RETAIN_POLICY_OBSOLETE_COUNT" -gt 0 ]; then
      echo "Removing $_APPLY_RETAIN_POLICY_OBSOLETE_COUNT oldest backups"
      if [ $_APPLY_RETAIN_POLICY_DIRECTORY_FORMAT = 1 ]; then
        find $MAIN_BACKUP_FOLDER/* -maxdepth 0 -type d -printf "%T@ %p\n" | \
          sort -nr | \
          tail -n +$((RETAIN_COUNT + 1)) | \
          awk '{ print $2 }' | \
          xargs -r rm -rf --
      else
        find $MAIN_BACKUP_FOLDER/* -maxdepth 0 -name "*$BACKUP_FILE_EXT" -type f -printf "%T@ %p\n" | \
          sort -nr | \
          tail -n +$((RETAIN_COUNT + 1)) | \
          awk '{ print $2 }' | \
          xargs -r rm --
      fi
    fi
  fi
}

on_backup_exit() {
  if [ -f $PID_FILE ]; then
    if [ -f "$(backup_path)" ] || [ -d "$(backup_path)" ]; then
      echo "Removing failed backup $(backup_path)"
      rm -rf "$(backup_path)"
    fi
    remove_lock
  else
    echo "Backup done"
  fi
}

create_lock() {
  echo $$ > "$PID_FILE"
}

remove_lock() {
  rm -f -- $PID_FILE
}

if [ -f $PID_FILE ]; then
   echo "Skipping. A backup is already in progress (pid: $(cat $PID_FILE))"
   exit 0
fi
create_lock

set -e
trap on_backup_exit 0