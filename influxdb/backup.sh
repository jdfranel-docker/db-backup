#!/bin/sh

# shellcheck disable=SC1091
. backup_base

INFLUXDB_PORT=${INFLUXDB_PORT:=8086}
INFLUXDB_HOST=${INFLUXDB_HOST:=db}
# shellcheck disable=SC2034
BACKUP_FORMAT=directory
#BACKUP_FORMAT=${$INFLUXDB_BACKUP_FORMAT:=directory}

if [ -z "$INFLUXDB_ADMIN_TOKEN" ]; then
  echo "Missing environment variable \$INFLUXDB_ADMIN_TOKEN"
  exit 34
fi

BACKUP_FILE_EXT="" # format: directory
BACKUP_FILE_NAME="$BACKUP_DATE-influxdb$BACKUP_FILE_EXT"

BACKUP_DESCIPTION=""

BUCKET=""
if [ -n "$INFLUXDB_BUCKET" ]; then
  BUCKET="--bucket $INFLUXDB_BUCKET"
  BACKUP_DESCIPTION="bucket: $INFLUXDB_BUCKET"
  # shellcheck disable=SC2034
  BACKUP_FILE_NAME="$BACKUP_DATE-influxdb-$INFLUXDB_BUCKET$BACKUP_FILE_EXT"
fi

ORG=""
if [ -n "$INFLUXDB_ORG" ]; then
  ORG="--org $INFLUXDB_ORG"
  if [ -n "$BACKUP_DESCIPTION" ]; then
    BACKUP_DESCRIPTION="$BACKUP_DESCRIPTION, "
  fi
  BACKUP_DESCRIPTION="${BACKUP_DESCRIPTION}org: $INFLUXDB_ORG"
fi

if [ -n "$BACKUP_DESCRIPTION" ]; then
  BACKUP_DESCRIPTION=" ($BACKUP_DESCRIPTION)"
else
  BACKUP_DESCRIPTION=" (complete backup)"
fi

echo "Backing up database$BACKUP_DESCRIPTION on host $INFLUXDB_HOST:$INFLUXDB_PORT. Saving on $(backup_path)"
influx backup $BUCKET $ORG --host http://$INFLUXDB_HOST:$INFLUXDB_PORT --token $INFLUXDB_ADMIN_TOKEN "$(backup_path)"

on_backup_success
