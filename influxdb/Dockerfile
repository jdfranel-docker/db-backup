FROM alpine:3.17

LABEL maintainer="jd@franel.me"

ENV CRON_SCHEDULE="0 3 * * *"

RUN apk add --no-cache tzdata findutils

COPY base/docker-entrypoint.sh /usr/local/bin/docker-entrypoint
RUN chmod +x /usr/local/bin/docker-entrypoint

ENTRYPOINT [ "docker-entrypoint" ]

COPY base/backup.sh /usr/local/bin/backup_base
COPY influxdb/backup.sh /usr/local/bin/backup
RUN chmod +x /usr/local/bin/backup_base /usr/local/bin/backup

COPY base/restore.sh /usr/local/bin/restore_base
COPY influxdb/restore.sh /usr/local/bin/restore
RUN chmod +x /usr/local/bin/restore_base /usr/local/bin/restore

CMD ["crond", "-f", "-l", "2"]

# Platform specific
ENV DATABASE_TYPE=influxdb
ARG INFLUXDB_CLIENT_VERSION=2.7.0
ADD https://repos.influxdata.com/influxdata-archive_compat.key influxdb2/influxdb2.key
ADD https://dl.influxdata.com/influxdb/releases/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz influxdb2/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz
ADD https://dl.influxdata.com/influxdb/releases/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz.asc influxdb2/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz.asc
RUN apk add --no-cache gnupg && \
    cat influxdb2/influxdb2.key | gpg --import - && \
    gpg --status-fd 1 --logger-fd 1 --verify influxdb2/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz.asc influxdb2/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz 2>/dev/null | grep 'gpg: Good signature from "InfluxData Package Signing Key <support@influxdata.com>" \[unknown\]' \
      || exit 1

RUN tar -xzf ./influxdb2/influxdb2-client-${INFLUXDB_CLIENT_VERSION}-linux-amd64.tar.gz -C influxdb2 && \
    cp influxdb2/influx /usr/local/bin/ && \
    rm -rf influxdb2
