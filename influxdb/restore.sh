#!/bin/sh

# shellcheck disable=SC1091
. restore_base

INFLUXDB_PORT=${INFLUXDB_PORT:=8086}
INFLUXDB_HOST=${INFLUXDB_HOST:=db}
# shellcheck disable=SC2034
BACKUP_FORMAT=directory
#BACKUP_FORMAT=${$INFLUXDB_BACKUP_FORMAT:=directory}

if [ -z "$INFLUXDB_ADMIN_TOKEN" ]; then
  echo "Missing environment variable \$INFLUXDB_ADMIN_TOKEN"
  exit 34
fi

# shellcheck disable=SC2034
RESTORE_FILE_EXT=""

RESTORE_DESCRIPTION=""
append_to_restore_description() {
  if [ -n "$RESTORE_DESCRIPTION" ]; then
    RESTORE_DESCRIPTION="$RESTORE_DESCRIPTION, "
  fi
  RESTORE_DESCRIPTION="${RESTORE_DESCRIPTION}$1"
}

FULL="--full"
#while getopts "f:b:n:l" option
while getopts "f:" option
do
  case $option in
    f)
      SELECTED_BACKUP=$OPTARG
      ;;
    # b)
    #   BUCKET="--bucket $OPTARG"
    #   append_to_restore_description "bucket: $OPTARG"
    #   ;;
    # n)
    #   NEW_BUCKET="--new-bucket $OPTARG"
    #   append_to_restore_description "into bucket: $OPTARG"
    #   ;;
    # l)
    #   FULL="--full"
    #   append_to_restore_description "full restore"
    *)
      ;;
  esac
done

init_file_to_restore "$SELECTED_BACKUP"
check_dump_exists
pre_process_backup

# if [ -z "$BUCKET" ] && [ -n "$INFLUXDB_BUCKET" ]; then
if [ -n "$INFLUXDB_BUCKET" ]; then
  BUCKET="--bucket $INFLUXDB_BUCKET"
  append_to_restore_description "bucket: $INFLUXDB_BUCKET"
  FULL=""
fi

ORG=""
if [ -n "$INFLUXDB_ORG" ]; then
  ORG="--org $INFLUXDB_ORG"
  append_to_restore_description "org: $INFLUXDB_ORG"
fi

if [ -n "$RESTORE_DESCRIPTION" ]; then
  RESTORE_DESCRIPTION=" ($RESTORE_DESCRIPTION)"
fi

echo "Restoring$RESTORE_DESCRIPTION dump $RESTORE_FILE_NAME into database on host $INFLUXDB_HOST:$INFLUXDB_PORT"
influx restore $BUCKET $ORG $FULL --host http://$INFLUXDB_HOST:$INFLUXDB_PORT --token $INFLUXDB_ADMIN_TOKEN $RESTORE_FILE_NAME
#influx restore $BUCKET $NEW_BUCKET $FULL --host http://$INFLUXDB_HOST:$INFLUXDB_PORT --token $INFLUXDB_ADMIN_TOKEN $RESTORE_FILE_NAME
echo "Restored $RESTORE_FILE_NAME"
