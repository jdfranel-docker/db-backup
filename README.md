# Database backup

> **IMPORTANT**\
> 🚚 Repository has moved to [jdfranel/db-backup](https://gitlab.com/jdfranel/db-backup).\
> The old repository is only here to redirect to the new one. Please update your git remote and/or your container registry to have the latest versions.\
> \
> *Update git remote with*
> ```
> git remote set-url origin git@gitlab.com:jdfranel/db-backup.git
> ```
> \
> *Update docker image to*
> ```
> registry.gitlab.com/jdfranel/db-backup/mysql:latest
> ```

This container allows you to backup and restore a database. It supports multiple database types.

By default, it runs a cron job that generate a database dump every day at 03:00. You can also backup manually by executing the command `backup`.

## Backup

### Cron job

You can change the cron periodicity by providing a `CRON_SCHEDULE` environment variable. Default value is `0 3 * * *`.

*See [crontab guru](https://crontab.guru/) for some help on crontab.*

### File permission

By default the database dumps are owned by `root:root` and chmod is `0644` (or `755` if the backup is a directory). You can change the owner by providing `FILE_UID` and `FILE_GID` environment variable and the chmod by providing `FILE_CHMOD` environment variable.

### Compression

To compress (gzip) the database dump, set the `COMPRESS` environment variable to `1`. By default the database dump is not compressed.

If the backup format is a directory (see specific database details), a tar archive will be created prior to compression.

### Retain policy

You can choose to keep only the last `N` database dump by providing a `RETAIN_COUNT` environment variable. By default, all database dumps are kept.

If a retain count is set, only files corresponding to the backup format will be removed.

### MySQL and MariaDB

Supports MySQL version 5.5 > 8 and MariaDB version 10.0 > 10.6.

You must use the following docker image: registry.gitlab.com/jdfranel-docker/db-backup/mysql:latest

#### Required environment variables for MySQL and MariaDB

* `MYSQL_DATABASE`
* `MYSQL_USER`
* `MYSQL_PASSWORD`

#### Optional environment variables for MySQL and MariaDB

* `MYSQL_PORT=3306`
* `MYSQL_HOST=db`

#### Limitations: MySQL native password

With MySQL 8 databases, you might encounter the following error:

```shell
mysqldump: Got error: 1045: "Plugin caching_sha2_password could not be loaded: Error loading shared library /usr/lib/mariadb/plugin/caching_sha2_password.so: No such file or directory" when trying to connect
```

To prevent this error from happening, the mysql server will needs to run with the `mysql_native_password` authentication plugin (add `--default_authentication_plugin=mysql_native_password` to the mysql running command).

#### Limitations: Tablespace dump

Since MySQL 5.7.31 and MySQL 8.0.21 you may encounter a *Access denied* error when running a `backup`.

```shell
mysqldump: Error: 'Access denied; you need (at least one of) the PROCESS privilege(s) for this operation' when trying to dump tablespaces
```

For this reason, by default, the `mysqldump` is done with `--no-tablespaces` option which is sufficient for most use cases. If you need to dump tablespaces, you can set the `MYSQL_DUMP_TABLESPACES=1` environment variable when running the `backup` command. You might also use the **root** username and password to ensure the `mysqldump` process have the necessary rights for dumping the tablespaces.

Read more about this error on this [anothercoffee-net article](https://anothercoffee.net/how-to-fix-the-mysqldump-access-denied-process-privilege-error/).

### PostgreSQL

Supports PostgreSQL version 9 > 15. The database dump will be a custom format dump compatible with the pg_restore command.

You can choose the backup format by setting the `POSTGRES_BACKUP_FORMAT` value to your preferred format. It is recommended to use the custom format since it allows multi-treaded data loading according to the number of CPU Cores you set for this container.

You must use the following docker image: registry.gitlab.com/jdfranel-docker/db-backup/postgres:latest

#### Required environment variables for PostgreSQL

* `POSTGRES_DB`
* `POSTGRES_USER`
* `POSTGRES_PASSWORD`

#### Optional environment variables for PostgreSQL

* `POSTGRES_PORT=5432`
* `POSTGRES_HOST=db`
* `POSTGRES_BACKUP_FORMAT=custom` \
  Supported formats are: custom, plain, directory, tar

#### Change ownership when restoring

When you imports a databse from another server and/or the `POSTGRES_USER` has changed, you might encounter the following error:

```shell
pg_restore: error: could not execute query: ERROR:  role "postgres_user" does not exist
```

To avoid this error, you can use the `-c` option when executing a restore command. It will execute the restore process and ignore the setting of the ownership from the backup and force the ownership to the `POSTGRES_USER`.

### MongoDB

Supports MongoDB version 3 > 6. The backup method uses `mongodump` for a single db.

You must use the following docker image: registry.gitlab.com/jdfranel-docker/db-backup/mongodb:latest

*Note from [MongoDB backup and restore tutorial](https://docs.mongodb.com/manual/tutorial/backup-and-restore-tools/)*

> mongodump and mongorestore cannot be part of a backup strategy for 4.2+ sharded clusters that have sharded transactions in progress, as backups created with mongodump do not maintain the atomicity guarantees of transactions across shards.

#### Required environment variables for MongoDB

* `MONGO_DB`
* `MONGO_USER`
* `MONGO_PASSWORD`

#### Optional environment variables for MongoDB

* `MONGO_PORT=27017`
* `MONGO_HOST=db`
* `MONGO_AUTH_DB=admin`
* `MONGO_BACKUP_FORMAT=archive` \
  Supported formats are: archive, directory

#### Limitations: Changing MongoDB database name

When you restore the MongoDB, the database name will also be restored from the dump.

### InfluxDB

Supports InfluxDB version 2.0.

You must use the following docker image: registry.gitlab.com/jdfranel-docker/db-backup/influxdb:latest

If you do not set the `INFLUXDB_BUCKET` you will backup and [restore the full database](https://docs.influxdata.com/influxdb/v2.0/reference/cli/influx/restore/#restore-and-replace-all-data).

#### Required environment variables for InfluxDB

* `INFLUXDB_ADMIN_TOKEN`

#### Optional environment variables for InfluxDB

* `INFLUXDB_PORT=8086`
* `INFLUXDB_HOST=db`
* `INFLUXDB_BUCKET=`

## Restore

### Restoring last backup

To restore the latest database backup in the `/dbdump` directory, execute the `restore` command from the db-backup container.

### Restoring a specific backup

To restore a specific database backup in the `/dbdump` directory, execute the `restore -f backup-name.ext` command from the db-backup container.

### PostgreSQL: Changing ownership when restoring database

When you imports a databse from another server with a different username, you might encounter the following error:

```shell
pg_restore: error: could not execute query: ERROR:  role "postgres_user" does not exist
```

To avoid this error, you can use the command `restore -c`. It will ignore the setting of the ownership when retoring the backup and force the ownership to the `POSTGRES_USER`.

<!-- ### Influx: Restoring a specific bucket into a new bucket

To restore a specific bucket from the database backup , execute the `restore -b original-bucket -n new-bucket` command from the db-backup container.

If the environment variable `INFLUXDB_BUCKET` is set, and the option `-b` is not set, the original bucket will be set to `INFLUXDB_BUCKET` by default.

[InfluxDB documentation: Restore backup data for a specific bucket into a new bucket](https://docs.influxdata.com/influxdb/v2.0/reference/cli/influx/restore/#restore-backup-data-for-a-specific-bucket-into-a-new-bucket)

### Influx: Full Restore

To [restore and replace all data](https://docs.influxdata.com/influxdb/v2.0/reference/cli/influx/restore/#restore-and-replace-all-data) use the `restore -l` command. -->

## Error codes

### Backup errors

| Code | Description |
| - | - |
| 10 | Unknown database type |
| 20 | Backup file has not been created |
| 21 | Created backup file is empty (size 0) |
| 22 | Created backup directory is empty (size 4) |
| 31 | Missing environment variables for MySQL |
| 32 | Missing environment variables for PostgreSQL |
| 33 | Missing environment variables for MongoDB |

### Restore errors

| Code | Description |
| - | - |
| 10 | Unknown database type |
| 11 | No dump to restore |
| 12 | Dump directory does not exists (for databases with directory backup format) |
| 13 | Dump file does not exists |
| 31 | Missing environment variables for MySQL |
| 32 | Missing environment variables for PostgreSQL |
| 33 | Missing environment variables for MongoDB |
| 34 | Missing environment variables for InfluxDB |

## TODO

* *influxdb*: [Allowing to restore a bucket into a new bucket](https://docs.influxdata.com/influxdb/v2.0/reference/cli/influx/restore/#restore-backup-data-for-a-specific-bucket-into-a-new-bucket)
* *influxdb*: Fix error when restoring a backup into an influxdb 2.1 server currently throwing a error `failed to restore SQL snapshot: InfluxDB OSS-only command failed: 500 Internal Server Error: An internal error has occurred - check server logs`
* Allowing to automatically restore the database on another database (cloning)
* Allowing to disable cron (only start crond if `CRON_SCHEDULE` has a value)
* Allowing to override cron command by setting container command (crond will be launched by the entrypoint)

## License

MIT

## Donate

If this tool has allowed you to spare a fair amount of time handling your database backups, please consider to offer me a coffee or, even better, a beer.

* [Buy me a coffee](https://www.buymeacoffee.com/oT35gfqDv)
* BTC : bc1q4v4rechtedld2zl5g080vj0flryklanyehyt30
* ETH : 0x98A943aC7D8148e746b549E512F5ACfdc91939dC
* LTC : ltc1qk4f4tg3aqavrguqvu05kfa86q294nm3s5k3g40
* XRP : rPqjg6474S9frY13Tw5eugF6aRowFEiR5Q
* XLM : GBPUYXF6UWWIJXS7AXTDRHVM4OL2ZSROJAKVCPDG5EDX6PFVIG6UVVJH
