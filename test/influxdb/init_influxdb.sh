#!/bin/sh
DATE=$(date +%s)
influx write \
  -b $DOCKER_INFLUXDB_INIT_BUCKET \
  -o $DOCKER_INFLUXDB_INIT_ORG \
  -p s \
  "myMeasurement,host=testHost testStringField=\"testString1\",testIntegerField=3,testFloatField=1.458,testBoolField=true $DATE"

DATE=$(($DATE + 10))
influx write \
  -b $DOCKER_INFLUXDB_INIT_BUCKET \
  -o $DOCKER_INFLUXDB_INIT_ORG \
  -p s \
  "myMeasurement,host=testHost testStringField=\"testString2\",testIntegerField=9,testFloatField=18.004,testBoolField=false $DATE"

DATE=$(($DATE + 10))
influx write \
  -b $DOCKER_INFLUXDB_INIT_BUCKET \
  -o $DOCKER_INFLUXDB_INIT_ORG \
  -p s \
  "myMeasurement,host=testHost testStringField=\"testString3\",testIntegerField=10,testFloatField=-4.147,testBoolField=false $DATE"