#!/bin/bash

testinfluxdb() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=influxdb
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_ORG=test_org
  local DB_BUCKET_NAME=test_bucket
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password
  local DB_ADMIN_TOKEN=this_is_an_authentication_token

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_RESTORE_CONTAINER_NAME=${INSTANCE}_test_db_restore_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}
  local DB_FRESH_CONTAINER_NAME=${INSTANCE}_test_db_fresh_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=40
  local DB_RUN_COMMAND=""

  title "Testing InfluxDB with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env DOCKER_INFLUXDB_INIT_MODE=setup \
    --env DOCKER_INFLUXDB_INIT_USERNAME=$DB_USERNAME \
    --env DOCKER_INFLUXDB_INIT_PASSWORD=$DB_PASSWORD \
    --env DOCKER_INFLUXDB_INIT_BUCKET=$DB_BUCKET_NAME \
    --env DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
    --env DOCKER_INFLUXDB_INIT_ORG=$DB_ORG \
    --volume $SCRIPTPATH/$VERSION/init_influxdb.sh:/docker-entrypoint-initdb.d/init_influxdb.sh:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  docker run \
    --name=$DB_BACKUP_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env INFLUXDB_HOST=$DB_INITIAL_CONTAINER_NAME \
    --env INFLUXDB_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    backup

  stopcontainers $DB_INITIAL_CONTAINER_NAME
  registercontainer $DB_FRESH_CONTAINER_NAME

  subtitle "Creating fresh database $DB_FRESH_CONTAINER_NAME"
  #DB_OLB_BUCKET_NAME=$DB_BUCKET_NAME
  #DB_BUCKET_NAME=test_bucket_fresh
  DB_USERNAME=test_user_fresh
  DB_PASSWORD=test_password_fresh

  docker run \
    --name=$DB_FRESH_CONTAINER_NAME \
    --hostname=$DB_FRESH_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env DOCKER_INFLUXDB_INIT_MODE=setup \
    --env DOCKER_INFLUXDB_INIT_USERNAME=$DB_USERNAME \
    --env DOCKER_INFLUXDB_INIT_PASSWORD=$DB_PASSWORD \
    --env DOCKER_INFLUXDB_INIT_BUCKET=$DB_BUCKET_NAME \
    --env DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
    --env DOCKER_INFLUXDB_INIT_ORG=$DB_ORG \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Restoring backup"
  docker run \
    --name=$DB_RESTORE_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env INFLUXDB_HOST=$DB_FRESH_CONTAINER_NAME \
    --env INFLUXDB_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    restore
    # restore -b $DB_OLB_BUCKET_NAME -n $DB_BUCKET_NAME

  subtitle "Testing data"

  testrequest() {
    local RESULT
    RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "influx query '$*'")
    echo $RESULT
  }

  local TEST1
  TEST1=$(testrequest "buckets()")
  if [[ -z $(echo "$TEST1" | grep "$DB_BUCKET_NAME") ]]; then
    echo "[FAILED] Test1: Bucket exists"
    echo "         Request returned $TEST1"
    echo "         Expected result to contain: '$DB_BUCKET_NAME'"
    exit 1
  else
    echo "[PASSED] Test1: Bucket '$DB_BUCKET_NAME' exists"
  fi

  local TEST2_EXPECTED_RESULT
  TEST2=$(testrequest "from(bucket: \"$DB_BUCKET_NAME\") |> range(start: -1y) |> keep(columns: [\"host\"]) |> group() |> count(column: \"host\")")
  local TEST2_EXPECTED_RESULT="12"
  if [[ $(echo $TEST2 | rev | cut -d ' ' -f 1 | rev) != "$TEST2_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test2: Table has $TEST2_EXPECTED_RESULT entries"
    echo "         Request returned $TEST2"
    echo "         Expected: '$TEST2_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test2: Table has $TEST2_EXPECTED_RESULT entries"
  fi

  local TEST3
  TEST3=$(testrequest "from(bucket: \"$DB_BUCKET_NAME\") |> range(start: -1y) |> filter(fn: (r) => r._measurement == \"myMeasurement\" and r.host == \"testHost\" and r._field == \"testStringField\" ) |> last()")
  if [[ -z $(echo $TEST3 | grep 'testString3') ]]; then
    echo "[FAILED] Test3: Last entry of testStringField has text 'testString3'"
    echo "         Request returned $TEST3"
    echo "         Expected result to contain: 'testString3'"
    exit 1
  else
    echo "[PASSED] Test3: Last entry of testStringField has text 'testString3'"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}

testinfluxdbretainpolicy() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=influxdb
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_ORG=test_org
  local DB_BUCKET_NAME=test_bucket
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password
  local DB_ADMIN_TOKEN=this_is_an_authentication_token

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=40
  local DB_RUN_COMMAND=""

  title "Testing InfluxDB with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env DOCKER_INFLUXDB_INIT_MODE=setup \
    --env DOCKER_INFLUXDB_INIT_USERNAME=$DB_USERNAME \
    --env DOCKER_INFLUXDB_INIT_PASSWORD=$DB_PASSWORD \
    --env DOCKER_INFLUXDB_INIT_BUCKET=$DB_BUCKET_NAME \
    --env DOCKER_INFLUXDB_INIT_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
    --env DOCKER_INFLUXDB_INIT_ORG=$DB_ORG \
    --volume $SCRIPTPATH/$VERSION/init_influxdb.sh:/docker-entrypoint-initdb.d/init_influxdb.sh:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  local X=0;
  while [ $X -lt 3 ]; do
    docker run \
      --name=$DB_BACKUP_CONTAINER_NAME \
      --rm \
      --network=$NETWORK \
      --env INFLUXDB_HOST=$DB_INITIAL_CONTAINER_NAME \
      --env INFLUXDB_ADMIN_TOKEN=$DB_ADMIN_TOKEN \
      --env FILE_UID="$(id -u)" \
      --env FILE_GID="$(id -g)" \
      --env RETAIN_COUNT=2 \
      $BACKUP_ENV \
      --volume $SCRIPTPATH/output:/dbdump \
      $DB_BACKUP_IMAGE_TAG \
      backup
    sleep 1
    X=$((X + 1))
  done

  stopcontainers $DB_INITIAL_CONTAINER_NAME

  subtitle "Testing data"
  local TEST1
  TEST1=$(find $SCRIPTPATH/output/* -maxdepth 0 | wc -l)
  if [[ ! $TEST1 -eq 2 ]]; then
    echo "[FAILED] Test1: retained backups count"
    echo "         Request returned $TEST1"
    echo "         Expected to be 2"
    exit 1
  else
    echo "[PASSED] Test1: retained backups count"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}


# Format: directory
testinfluxdb influxdb:2.0
#testinfluxdb influxdb:2.1
testinfluxdbretainpolicy influxdb:2.0

# Format: directory compressed
testinfluxdb influxdb:2.0 "--env COMPRESS=1"
#testinfluxdb influxdb:2.1 "--env COMPRESS=1"
testinfluxdbretainpolicy influxdb:2.0 "--env COMPRESS=1"