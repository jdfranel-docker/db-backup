#!/bin/bash

testmysql() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=mysql
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_ROOT_PASSWORD=test_root_password
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_RESTORE_CONTAINER_NAME=${INSTANCE}_test_db_restore_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}
  local DB_FRESH_CONTAINER_NAME=${INSTANCE}_test_db_fresh_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=30
  local DB_RUN_COMMAND=""

  title "Testing SQL with image $DB_IMAGE_TAG ($BACKUP_ENV)"
  if [[ $(echo $DB_IMAGE_TAG | sed 's/^mysql\:8.*/MATCH/') == "MATCH" ]]; then
    DB_RUN_COMMAND="--default_authentication_plugin=mysql_native_password"
  fi
  if [[ $(echo $DB_IMAGE_TAG | sed 's/^mariadb\:10.0/MATCH/') == "MATCH" ]] || \
    [[ $(echo $DB_IMAGE_TAG | sed 's/^mariadb\:10.1/MATCH/') == "MATCH" ]]
  then
    WAIT_DURATION=20
  fi

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MYSQL_ROOT_PASSWORD=$DB_ROOT_PASSWORD \
    --env MYSQL_DATABASE=$DB_DATABASE_NAME \
    --env MYSQL_USER=$DB_USERNAME \
    --env MYSQL_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_mysql.sql:/docker-entrypoint-initdb.d/init.sql:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  docker run \
    --name=$DB_BACKUP_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env MYSQL_HOST=$DB_INITIAL_CONTAINER_NAME \
    --env MYSQL_DATABASE=$DB_DATABASE_NAME \
    --env MYSQL_USER=$DB_USERNAME \
    --env MYSQL_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    backup

  stopcontainers $DB_INITIAL_CONTAINER_NAME
  registercontainer $DB_FRESH_CONTAINER_NAME

  subtitle "Creating fresh database $DB_FRESH_CONTAINER_NAME"
  DB_DATABASE_NAME=test_db_fresh
  DB_ROOT_PASSWORD=test_root_password_fresh
  DB_USERNAME=test_user_fresh
  DB_PASSWORD=test_password_fresh

  docker run \
    --name=$DB_FRESH_CONTAINER_NAME \
    --hostname=$DB_FRESH_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MYSQL_ROOT_PASSWORD=$DB_ROOT_PASSWORD \
    --env MYSQL_DATABASE=$DB_DATABASE_NAME \
    --env MYSQL_USER=$DB_USERNAME \
    --env MYSQL_PASSWORD=$DB_PASSWORD \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Restoring backup"
  set +e
  docker run \
    --name=$DB_RESTORE_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env MYSQL_HOST=$DB_FRESH_CONTAINER_NAME \
    --env MYSQL_DATABASE=$DB_DATABASE_NAME \
    --env MYSQL_USER=$DB_USERNAME \
    --env MYSQL_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    restore
  set -e

  subtitle "Testing data"

  testmysqlquery() {
    local RESULT
    RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "mysql -u \$MYSQL_USER -p\$MYSQL_PASSWORD \$MYSQL_DATABASE -e \"$*\"")
    echo $RESULT
  }

  local TEST1
  TEST1=$(testmysqlquery "SHOW TABLES;")
  if [[ -z $(echo "$TEST1" | grep 'test_table') ]]; then
    echo "[FAILED] Test1: Table exists"
    echo "         Request returned $TEST1"
    echo "         Expected to contain 'test_table'"
    exit 1
  else
    echo "[PASSED] Test1: Table exists"
  fi

  local TEST2
  TEST2=$(testmysqlquery "SELECT COUNT(id) AS count FROM test_table;")
  local TEST2_EXPECTED_RESULT="count 3"
  if [[ $TEST2 != "$TEST2_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test2: Table has 3 entries"
    echo "         Request returned $TEST2"
    echo "         Expected: '$TEST2_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test2: Table has 3 entries"
  fi

  local TEST3
  TEST3=$(testmysqlquery "SELECT text FROM test_table WHERE id = 2;")
  local TEST3_EXPECTED_RESULT="text another text"
  if [[ $TEST3 != "$TEST3_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test3: Entry with id 2 has text 'another text'"
    echo "         Request returned $TEST3"
    echo "         Expected: '$TEST3_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test3: Entry with id 2 has text 'another text'"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}

testmysqlretainpolicy() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=mysql
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_ROOT_PASSWORD=test_root_password
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=30
  local DB_RUN_COMMAND=""

  title "Testing retain policy with image $DB_IMAGE_TAG ($BACKUP_ENV)"
  if [[ $(echo $DB_IMAGE_TAG | sed 's/^mysql\:8.*/MATCH/') == "MATCH" ]]; then
    DB_RUN_COMMAND="--default_authentication_plugin=mysql_native_password"
  fi
  if [[ $(echo $DB_IMAGE_TAG | sed 's/^mariadb\:10.0/MATCH/') == "MATCH" ]] || \
    [[ $(echo $DB_IMAGE_TAG | sed 's/^mariadb\:10.1/MATCH/') == "MATCH" ]]
  then
    WAIT_DURATION=20
  fi

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MYSQL_ROOT_PASSWORD=$DB_ROOT_PASSWORD \
    --env MYSQL_DATABASE=$DB_DATABASE_NAME \
    --env MYSQL_USER=$DB_USERNAME \
    --env MYSQL_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_mysql.sql:/docker-entrypoint-initdb.d/init.sql:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backups"
  local X=0;
  while [ $X -lt 3 ]; do
    docker run \
      --name=$DB_BACKUP_CONTAINER_NAME \
      --rm \
      --network=$NETWORK \
      --env MYSQL_HOST=$DB_INITIAL_CONTAINER_NAME \
      --env MYSQL_DATABASE=$DB_DATABASE_NAME \
      --env MYSQL_USER=$DB_USERNAME \
      --env MYSQL_PASSWORD=$DB_PASSWORD \
      --env FILE_UID="$(id -u)" \
      --env FILE_GID="$(id -g)" \
      --env RETAIN_COUNT=2 \
      $BACKUP_ENV \
      --volume $SCRIPTPATH/output:/dbdump \
      $DB_BACKUP_IMAGE_TAG \
      backup
    sleep 1
    X=$((X + 1))
  done

  stopcontainers $DB_INITIAL_CONTAINER_NAME

  subtitle "Testing data"
  local TEST1
  TEST1=$(find $SCRIPTPATH/output/* -maxdepth 0 | wc -l)
  if [[ ! $TEST1 -eq 2 ]]; then
    echo "[FAILED] Test1: retained backups count"
    echo "         Request returned $TEST1"
    echo "         Expected to be 2"
    exit 1
  else
    echo "[PASSED] Test1: retained backups count"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}

# Format: plain
testmysql mysql:5.5
testmysql mysql:5.6
testmysql mysql:5.7
testmysql mysql:8.0
testmysql mariadb:10.0
testmysql mariadb:10.1
testmysql mariadb:10.2
testmysql mariadb:10.3
testmysql mariadb:10.4
testmysql mariadb:10.5
testmysql mariadb:10.6
testmysqlretainpolicy mysql:5.5 "--env COMPRESS=1"

# Format: plain compressed
testmysql mysql:5.5 "--env COMPRESS=1"
testmysql mysql:5.6 "--env COMPRESS=1"
testmysql mysql:5.7 "--env COMPRESS=1"
testmysql mysql:8.0 "--env COMPRESS=1"
testmysql mariadb:10.0 "--env COMPRESS=1"
testmysql mariadb:10.1 "--env COMPRESS=1"
testmysql mariadb:10.2 "--env COMPRESS=1"
testmysql mariadb:10.3 "--env COMPRESS=1"
testmysql mariadb:10.4 "--env COMPRESS=1"
testmysql mariadb:10.5 "--env COMPRESS=1"
testmysql mariadb:10.6 "--env COMPRESS=1"
testmysqlretainpolicy mysql:5.5
