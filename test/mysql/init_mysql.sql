CREATE TABLE `test_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `boolean` int(1) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `test_table` VALUES (1,10,'a text',1),(2,20,'another text',0),(3,30,'more text',1);
