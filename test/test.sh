#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

INSTANCE=${RANDOM}_$(date +"%Y%m%d%H%M%S")

NETWORK=${INSTANCE}_test_db_backup_network
CONTAINERS=""

DB_BACKUP_IMAGE_NAME=${INSTANCE}_test_db_backup

set -e

title() {
  echo ""
  echo "********************************************************"
  echo "* $1"
  echo "********************************************************"
}

subtitle() {
  echo "** $1"
}

notice() {
  echo "*** $1"
}

## Countdown (by Jean-Daniel Franel)
# Usage:
#   countdown [duration > 0] [progress_factor (default: 1)]
# Exemples:
#   countdown 10 # countdown to 10 with progress bar of length 10
#   countdown 100 10 # countdown to 100 with progress bar of length 100/10
countdown() {
  DURATION=$1
  PROGRESS_FACTOR=${2:-1}
  if [ "$DURATION" -lt 1 ]; then
    echo "Error: Invalid countdown duration ($DURATION)"
    exit 1
  fi
  LENGTH=${#DURATION}
  echo "Waiting $DURATION seconds"
  for (( COUNTDOWN=DURATION; COUNTDOWN>0; COUNTDOWN-- )); do
    BAR_LENGTH=$(( (DURATION + PROGRESS_FACTOR - 1) / PROGRESS_FACTOR ))
    BAR_PROGRESS=$(( (DURATION - COUNTDOWN + PROGRESS_FACTOR - 1) / PROGRESS_FACTOR ))
    for (( PROGRESS=0; PROGRESS<BAR_PROGRESS; PROGRESS++ )); do
      echo -ne "*"
    done
    for (( BAR=0; BAR<(BAR_LENGTH-BAR_PROGRESS); BAR++ )); do
      echo -ne "·"
    done
    printf -v FORMATTED_COUNTDOWN "%0${LENGTH}d" "$COUNTDOWN"
    echo -ne " ${FORMATTED_COUNTDOWN}s\033[0K\r"
    sleep 1
  done
  echo -ne "\033[0K\r"
}

registercontainer() {
  CONTAINERS="$CONTAINERS $1"
}

finddockerimage() {
  docker image ls -q "$1"
}

finddockernetwork() {
  docker network ls -qf "name=$1"
}

teardown() {
  title "Tearing down"
  stopcontainers "$CONTAINERS"
  local NETWORK_ID
  NETWORK_ID=$(finddockernetwork "$NETWORK")
  if [[ -n $NETWORK_ID ]]; then
    subtitle "Stopping network $NETWORK"
    docker network remove "$NETWORK_ID"
  fi
  subtitle "Removing images"
  local IMAGES
  IMAGES=$(finddockerimage "$DB_BACKUP_IMAGE_NAME")
  if [[ -n $IMAGES ]]; then
    for IMAGE in $IMAGES; do
      notice "$IMAGE"
      docker image rm -f "$IMAGE"
    done
  fi
  rm -rf "$SCRIPTPATH"/output
}

emptyoutputdirectory() {
  rm -rf "$SCRIPTPATH"/output/*
}

stopcontainers() {
  subtitle "Stopping containers $*"
  if [[ -n $* ]]; then
    # shellcheck disable=SC2068
    for CONTAINER in $@; do
      local CONTAINER_ID
      CONTAINER_ID=$(docker ps -aqf "name=$CONTAINER")
      if [ -n "$CONTAINER_ID" ]; then
        notice "$CONTAINER"
        docker stop "$CONTAINER_ID"
      fi
    done
  fi
}

buildimage() {
  local VERSION=$1
  local IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION
  subtitle "Building db-backup image $IMAGE_TAG"
  if [ -z "$(finddockerimage "$IMAGE_TAG")" ]; then
    docker build -t "$IMAGE_TAG" -f "$SCRIPTPATH/../$VERSION/Dockerfile" "$SCRIPTPATH/.."
  fi
}

trap "teardown" EXIT

title "Starting tests (instance $INSTANCE)"

mkdir "$SCRIPTPATH/output"

docker network create "$NETWORK"

if [[ -z $* ]] || echo "$@" | grep 'mysql'; then
  # shellcheck disable=SC1091
  source "$SCRIPTPATH/mysql/test.sh"
fi
if [[ -z $* ]] || echo "$@" | grep 'postgres'; then
  # shellcheck disable=SC1091
  source "$SCRIPTPATH/postgres/test.sh"
fi
if [[ -z $* ]] || echo "$@" | grep -q 'mongodb'; then
  # shellcheck disable=SC1091
  source "$SCRIPTPATH/mongodb/test.sh"
fi
if [[ -z $* ]] || echo "$@" | grep -q 'influxdb'; then
  # shellcheck disable=SC1091
  source "$SCRIPTPATH/influxdb/test.sh"
fi

echo ""
echo ""
echo "[PASSED] ALL tests"
