db.test_collection.insert({
  id: 1,
  number: 10,
  text: "a text",
  boolean: true
});

db.test_collection.insert({
  id: 2,
  number: 20,
  text: "another text",
  boolean: false
});

db.test_collection.insert({
  id: 3,
  number: 30,
  text: "more text",
  boolean: true
});