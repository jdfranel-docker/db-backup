#!/bin/bash

testmongodb() {
  local TARGET_VERSION=$1
  local DB_IMAGE_TAG=$2
  local BACKUP_ENV=$3
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=mongodb
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_RESTORE_CONTAINER_NAME=${INSTANCE}_test_db_restore_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}
  local DB_FRESH_CONTAINER_NAME=${INSTANCE}_test_db_fresh_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=5
  local DB_RUN_COMMAND=""

  title "Testing MongoDB with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MONGO_INITDB_DATABASE=$DB_DATABASE_NAME \
    --env MONGO_INITDB_ROOT_USERNAME=$DB_USERNAME \
    --env MONGO_INITDB_ROOT_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_mongodb.js:/docker-entrypoint-initdb.d/init_mongodb.js:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  docker run \
    --name=$DB_BACKUP_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env MONGO_HOST=$DB_INITIAL_CONTAINER_NAME \
    --env MONGO_DB=$DB_DATABASE_NAME \
    --env MONGO_USER=$DB_USERNAME \
    --env MONGO_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    backup

  stopcontainers $DB_INITIAL_CONTAINER_NAME
  registercontainer $DB_FRESH_CONTAINER_NAME

  subtitle "Creating fresh database $DB_FRESH_CONTAINER_NAME"
  # DB_DATABASE_NAME=test_db_fresh
  DB_USERNAME=test_user_fresh
  DB_PASSWORD=test_password_fresh

  docker run \
    --name=$DB_FRESH_CONTAINER_NAME \
    --hostname=$DB_FRESH_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MONGO_INITDB_DATABASE=$DB_DATABASE_NAME \
    --env MONGO_INITDB_ROOT_USERNAME=$DB_USERNAME \
    --env MONGO_INITDB_ROOT_PASSWORD=$DB_PASSWORD \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Restoring backup"
  docker run \
    --name=$DB_RESTORE_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env MONGO_HOST=$DB_FRESH_CONTAINER_NAME \
    --env MONGO_DB=$DB_DATABASE_NAME \
    --env MONGO_USER=$DB_USERNAME \
    --env MONGO_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    restore

  subtitle "Testing data ($BACKUP_ENV)"

  testrequest() {
    local RESULT
    if [[ $TARGET_VERSION -gt 4 ]]; then
      RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "mongosh --eval=\"$*\" --quiet --username=\$MONGO_INITDB_ROOT_USERNAME --password=\$MONGO_INITDB_ROOT_PASSWORD --host=127.0.0.1 --port=27017 --authenticationDatabase=admin \$MONGO_INITDB_DATABASE")
    else
      RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "mongo --eval=\"$*\" --quiet --username=\$MONGO_INITDB_ROOT_USERNAME --password=\$MONGO_INITDB_ROOT_PASSWORD --host=127.0.0.1 --port=27017 --authenticationDatabase=admin \$MONGO_INITDB_DATABASE")
    fi
    echo $RESULT
  }

  local TEST1
  TEST1=$(testrequest "db.getCollectionNames();")
  if [[ -z $(echo "$TEST1" | grep 'test_collection') ]]; then
    echo "[FAILED] Test1: Collection exists"
    echo "         Request returned $TEST1"
    echo "         Expected result to contain: 'test_collection'"
    exit 1
  else
    echo "[PASSED] Test1: Collection exists"
  fi

  local TEST2
  if [[ $TARGET_VERSION -gt 4 ]]; then
    TEST2=$(testrequest "db.test_collection.countDocuments()")
  else
    TEST2=$(testrequest "db.test_collection.count()")
  fi
  local TEST2_EXPECTED_RESULT="3"
  if [[ $TEST2 != "$TEST2_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test2: Table has 3 entries"
    echo "         Request returned $TEST2"
    echo "         Expected: '$TEST2_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test2: Table has 3 entries"
  fi

  local TEST3
  TEST3=$(testrequest "db.test_collection.find({\"id\": 2}).map(item => item.text)")
  if [[ -z $(echo $TEST3 | grep 'another text') ]]; then
    echo "[FAILED] Test3: Entry with id 2 has text 'another text'"
    echo "         Request returned $TEST3"
    echo "         Expected result to contain: 'another text'"
    exit 1
  else
    echo "[PASSED] Test3: Entry with id 2 has text 'another text'"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}

testmongodbretainpolicy() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=mongodb
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=5
  local DB_RUN_COMMAND=""

  title "Testing retain policy with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env MONGO_INITDB_DATABASE=$DB_DATABASE_NAME \
    --env MONGO_INITDB_ROOT_USERNAME=$DB_USERNAME \
    --env MONGO_INITDB_ROOT_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_mongodb.js:/docker-entrypoint-initdb.d/init_mongodb.js:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backups"
  local X=0;
  while [ $X -lt 3 ]; do
    docker run \
      --name=$DB_BACKUP_CONTAINER_NAME \
      --rm \
      --network=$NETWORK \
      --env MONGO_HOST=$DB_INITIAL_CONTAINER_NAME \
      --env MONGO_DB=$DB_DATABASE_NAME \
      --env MONGO_USER=$DB_USERNAME \
      --env MONGO_PASSWORD=$DB_PASSWORD \
      --env FILE_UID="$(id -u)" \
      --env FILE_GID="$(id -g)" \
      --env RETAIN_COUNT=2 \
      $BACKUP_ENV \
      --volume $SCRIPTPATH/output:/dbdump \
      $DB_BACKUP_IMAGE_TAG \
      backup
    sleep 1
    X=$((X + 1))
  done

  stopcontainers $DB_INITIAL_CONTAINER_NAME

  subtitle "Testing data"
  local TEST1
  TEST1=$(find $SCRIPTPATH/output/* -maxdepth 0 | wc -l)
  if [[ ! $TEST1 -eq 2 ]]; then
    echo "[FAILED] Test1: retained backups count"
    echo "         Request returned $TEST1"
    echo "         Expected to be 2"
    exit 1
  else
    echo "[PASSED] Test1: retained backups count"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}


# format: archive
testmongodb 3 mongo:3
testmongodb 4 mongo:4
testmongodb 5 mongo:5
testmongodb 6 mongo:6
testmongodbretainpolicy mongo:6

# format: archive compressed
testmongodb 3 mongo:3 "--env COMPRESS=1"
testmongodb 4 mongo:4 "--env COMPRESS=1"
testmongodb 5 mongo:5 "--env COMPRESS=1"
testmongodb 6 mongo:6 "--env COMPRESS=1"
testmongodbretainpolicy mongo:6 "--env COMPRESS=1"

# format: directory
testmongodb 3 mongo:3 "--env MONGO_BACKUP_FORMAT=directory"
testmongodb 4 mongo:4 "--env MONGO_BACKUP_FORMAT=directory"
testmongodb 5 mongo:5 "--env MONGO_BACKUP_FORMAT=directory"
testmongodb 6 mongo:6 "--env MONGO_BACKUP_FORMAT=directory"
testmongodbretainpolicy mongo:6 "--env MONGO_BACKUP_FORMAT=directory"

# format: directory compressed
testmongodb 3 mongo:3 "--env MONGO_BACKUP_FORMAT=directory --env COMPRESS=1"
testmongodb 4 mongo:4 "--env MONGO_BACKUP_FORMAT=directory --env COMPRESS=1"
testmongodb 5 mongo:5 "--env MONGO_BACKUP_FORMAT=directory --env COMPRESS=1"
testmongodb 6 mongo:6 "--env MONGO_BACKUP_FORMAT=directory --env COMPRESS=1"
testmongodbretainpolicy mongo:6 "--env MONGO_BACKUP_FORMAT=directory --env COMPRESS=1"