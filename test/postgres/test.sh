#!/bin/bash

testpostgresql() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=postgres
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_RESTORE_CONTAINER_NAME=${INSTANCE}_test_db_restore_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}
  local DB_FRESH_CONTAINER_NAME=${INSTANCE}_test_db_fresh_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=5
  local DB_RUN_COMMAND=""

  title "Testing PostgreSQL with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env POSTGRES_DB=$DB_DATABASE_NAME \
    --env POSTGRES_USER=$DB_USERNAME \
    --env POSTGRES_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_postgres.sql:/docker-entrypoint-initdb.d/init.sql:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  docker run \
    --name=$DB_BACKUP_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env POSTGRES_HOST=$DB_INITIAL_CONTAINER_NAME \
    --env POSTGRES_DB=$DB_DATABASE_NAME \
    --env POSTGRES_USER=$DB_USERNAME \
    --env POSTGRES_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    backup

  stopcontainers $DB_INITIAL_CONTAINER_NAME
  registercontainer $DB_FRESH_CONTAINER_NAME

  subtitle "Creating fresh database $DB_FRESH_CONTAINER_NAME"
  DB_DATABASE_NAME=test_db_fresh
  DB_USERNAME=test_user_fresh
  DB_PASSWORD=test_password_fresh

  docker run \
    --name=$DB_FRESH_CONTAINER_NAME \
    --hostname=$DB_FRESH_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env POSTGRES_DB=$DB_DATABASE_NAME \
    --env POSTGRES_USER=$DB_USERNAME \
    --env POSTGRES_PASSWORD=$DB_PASSWORD \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Restoring backup"
  set +e
  docker run \
    --name=$DB_RESTORE_CONTAINER_NAME \
    --rm \
    --network=$NETWORK \
    --env POSTGRES_HOST=$DB_FRESH_CONTAINER_NAME \
    --env POSTGRES_DB=$DB_DATABASE_NAME \
    --env POSTGRES_USER=$DB_USERNAME \
    --env POSTGRES_PASSWORD=$DB_PASSWORD \
    --env FILE_UID="$(id -u)" \
    --env FILE_GID="$(id -g)" \
    $BACKUP_ENV \
    --volume $SCRIPTPATH/output:/dbdump \
    $DB_BACKUP_IMAGE_TAG \
    restore -c
  set -e

  subtitle "Testing data"

  testpostgresqlquery() {
    local RESULT
    RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "psql -c \"$*\" postgres://\$POSTGRES_USER:\$POSTGRES_PASSWORD@127.0.0.1/\$POSTGRES_DB")
    echo $RESULT
  }

  testpostgresqltuplesquery() {
    local RESULT
    RESULT=$(docker exec $DB_FRESH_CONTAINER_NAME sh -c "psql --tuples-only -c \"$*\" postgres://\$POSTGRES_USER:\$POSTGRES_PASSWORD@127.0.0.1/\$POSTGRES_DB")
    echo $RESULT
  }

  local TEST1
  TEST1=$(testpostgresqlquery "SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';")
  if [[ -z $(echo "$TEST1" | grep 'test_table') ]] ; then
    echo "[FAILED] Test1: Table exists"
    echo "         Request returned $TEST1"
    echo "         Expected to contain 'test_table'"
    exit 1
  else
    echo "[PASSED] Test1: Table exists"
  fi

  local TEST2
  TEST2=$(testpostgresqltuplesquery "SELECT COUNT(id) AS count FROM test_table;")
  local TEST2_EXPECTED_RESULT="3"
  if [[ $TEST2 != "$TEST2_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test2: Table has 3 entries"
    echo "         Request returned $TEST2"
    echo "         Expected: '$TEST2_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test2: Table has 3 entries"
  fi

  local TEST3
  TEST3=$(testpostgresqltuplesquery "SELECT text FROM test_table WHERE id = 2;")
  local TEST3_EXPECTED_RESULT="another text"
  if [[ $TEST3 != "$TEST3_EXPECTED_RESULT" ]]; then
    echo "[FAILED] Test3: Entry with id 2 has text 'another text'"
    echo "         Request returned $TEST3"
    echo "         Expected: '$TEST3_EXPECTED_RESULT'"
    exit 1
  else
    echo "[PASSED] Test3: Entry with id 2 has text 'another text'"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}

testpostgresqlretainpolicy() {
  local DB_IMAGE_TAG=$1
  local BACKUP_ENV=$2
  local CLEAN_IMAGE_TAG
  CLEAN_IMAGE_TAG=$(echo $DB_IMAGE_TAG | sed 's/[:\.]/_/g')
  local VERSION=postgres
  local DB_BACKUP_IMAGE_TAG=$DB_BACKUP_IMAGE_NAME:$VERSION

  local DB_DATABASE_NAME=test_db
  local DB_USERNAME=test_user
  local DB_PASSWORD=test_password

  local DB_BACKUP_CONTAINER_NAME=${INSTANCE}_test_db_backup_${CLEAN_IMAGE_TAG}
  local DB_INITIAL_CONTAINER_NAME=${INSTANCE}_test_db_initial_${CLEAN_IMAGE_TAG}

  local WAIT_DURATION=5
  local DB_RUN_COMMAND=""

  title "Testing retain policy with image $DB_IMAGE_TAG ($BACKUP_ENV)"

  buildimage $VERSION
  registercontainer $DB_INITIAL_CONTAINER_NAME

  subtitle "Creating database $DB_INITIAL_CONTAINER_NAME"
  docker run \
    --name=$DB_INITIAL_CONTAINER_NAME \
    --hostname=$DB_INITIAL_CONTAINER_NAME \
    --detach \
    --rm \
    --network=$NETWORK \
    --env POSTGRES_DB=$DB_DATABASE_NAME \
    --env POSTGRES_USER=$DB_USERNAME \
    --env POSTGRES_PASSWORD=$DB_PASSWORD \
    --volume $SCRIPTPATH/$VERSION/init_postgres.sql:/docker-entrypoint-initdb.d/init.sql:ro \
    $DB_IMAGE_TAG $DB_RUN_COMMAND

  countdown $WAIT_DURATION
  subtitle "Creating backup"
  local X=0;
  while [ $X -lt 3 ]; do
    docker run \
      --name=$DB_BACKUP_CONTAINER_NAME \
      --rm \
      --network=$NETWORK \
      --env POSTGRES_HOST=$DB_INITIAL_CONTAINER_NAME \
      --env POSTGRES_DB=$DB_DATABASE_NAME \
      --env POSTGRES_USER=$DB_USERNAME \
      --env POSTGRES_PASSWORD=$DB_PASSWORD \
      --env FILE_UID="$(id -u)" \
      --env FILE_GID="$(id -g)" \
      --env RETAIN_COUNT=2 \
      $BACKUP_ENV \
      --volume $SCRIPTPATH/output:/dbdump \
      $DB_BACKUP_IMAGE_TAG \
      backup
    sleep 1
    X=$((X + 1))
  done

  stopcontainers $DB_INITIAL_CONTAINER_NAME

  subtitle "Testing data"
  local TEST1
  TEST1=$(find $SCRIPTPATH/output/* -maxdepth 0 | wc -l)
  if [[ ! $TEST1 -eq 2 ]]; then
    echo "[FAILED] Test1: retained backups count"
    echo "         Request returned $TEST1"
    echo "         Expected to be 2"
    exit 1
  else
    echo "[PASSED] Test1: retained backups count"
  fi

  stopcontainers $DB_FRESH_CONTAINER_NAME
  emptyoutputdirectory
}


# Format: custom
testpostgresql postgres:9
testpostgresql postgres:10
testpostgresql postgres:11
testpostgresql postgres:12
testpostgresql postgres:13
testpostgresql postgres:14
testpostgresql postgres:15
testpostgresqlretainpolicy postgres:15

# Format: custom compressed
testpostgresql postgres:9 "--env COMPRESS=1"
testpostgresql postgres:10 "--env COMPRESS=1"
testpostgresql postgres:11 "--env COMPRESS=1"
testpostgresql postgres:12 "--env COMPRESS=1"
testpostgresql postgres:13 "--env COMPRESS=1"
testpostgresql postgres:14 "--env COMPRESS=1"
testpostgresql postgres:15 "--env COMPRESS=1"
testpostgresqlretainpolicy postgres:15 "--env COMPRESS=1"

# Format: tar
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=tar"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=tar"

# Format: tar compressed
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=tar --env COMPRESS=1"

# Format: plain
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=plain"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=plain"

# Format: plain compressed
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=plain --env COMPRESS=1"

# Format: directory
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=directory"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=directory"

# Format: directory compressed
testpostgresql postgres:9 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:10 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:11 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:12 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:13 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:14 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresql postgres:15 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
testpostgresqlretainpolicy postgres:15 "--env POSTGRES_BACKUP_FORMAT=directory --env COMPRESS=1"
