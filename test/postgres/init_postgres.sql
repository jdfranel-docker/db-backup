CREATE TABLE public.test_table (
  id smallint NOT NULL,
  number smallint NOT NULL,
  text character varying(255) NOT NULL,
  boolean boolean NOT NULL,
  CONSTRAINT test_table_okey PRIMARY KEY (id)
);

INSERT INTO public.test_table VALUES (1,10,'a text',true),(2,20,'another text',false),(3,30,'more text',true);
