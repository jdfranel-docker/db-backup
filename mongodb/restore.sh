#!/bin/sh

# shellcheck disable=SC1091
. restore_base

MONGO_PORT=${MONGO_PORT:=27017}
MONGO_HOST=${MONGO_HOST:=db}
MONGO_AUTH_DB=${MONGO_AUTH_DB:=admin}
BACKUP_FORMAT=${MONGO_BACKUP_FORMAT:=archive}

if [ -z "$MONGO_DB" ]; then
  echo "Missing environment variable \$MONGO_DB"
  exit 33
fi

if [ -z "$MONGO_USER" ]; then
  echo "Missing environment variable \$MONGO_USER"
  exit 33
fi

if [ -z "$MONGO_PASSWORD" ]; then
  echo "Missing environment variable \$MONGO_PASSWORD"
  exit 33
fi

SOURCE="--archive="

RESTORE_FILE_EXT=".archive"
if [ "$BACKUP_FORMAT" = "archive" ] && [ "$COMPRESS" = 1 ]; then
  RESTORE_FILE_EXT=".archive.gz"
  SOURCE="--gzip --archive="
  # shellcheck disable=SC2034
  SKIP_UNCOMPRESS=1
elif [ "$BACKUP_FORMAT" = "directory" ]; then
# shellcheck disable=SC2034
  RESTORE_FILE_EXT=""
  SOURCE=""
fi

while getopts "f:" option
do
  case $option in
    f)
      SELECTED_BACKUP=$OPTARG
      ;;
    *)
      ;;
  esac
done

init_file_to_restore "$SELECTED_BACKUP"
check_dump_exists
pre_process_backup

echo "Restoring dump $RESTORE_FILE_NAME into database $MONGO_DB with user $MONGO_USER on host $MONGO_HOST:$MONGO_PORT"
mongorestore --host=$MONGO_HOST --port=$MONGO_PORT --username=$MONGO_USER --password=$MONGO_PASSWORD --authenticationDatabase=$MONGO_AUTH_DB --drop --preserveUUID $SOURCE$RESTORE_FILE_NAME
echo "Restored $RESTORE_FILE_NAME"
