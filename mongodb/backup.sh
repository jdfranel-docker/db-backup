#!/bin/sh

# shellcheck disable=SC1091
. backup_base

MONGO_PORT=${MONGO_PORT:=27017}
MONGO_HOST=${MONGO_HOST:=db}
MONGO_AUTH_DB=${MONGO_AUTH_DB:=admin}
BACKUP_FORMAT=${MONGO_BACKUP_FORMAT:=archive}

if [ -z "$MONGO_DB" ]; then
  echo "Missing environment variable \$MONGO_DB"
  exit 33
fi

if [ -z "$MONGO_USER" ]; then
  echo "Missing environment variable \$MONGO_USER"
  exit 33
fi

if [ -z "$MONGO_PASSWORD" ]; then
  echo "Missing environment variable \$MONGO_PASSWORD"
  exit 33
fi

OUTPUT="--archive"

BACKUP_FILE_EXT=".archive"  # format: archive
if [ "$BACKUP_FORMAT" = "archive" ] && [ "$COMPRESS" = "1" ]; then
  BACKUP_FILE_EXT=".archive.gz"
  OUTPUT="--gzip --archive"
  # shellcheck disable=SC2034
  SKIP_COMPRESS=1
elif [ "$BACKUP_FORMAT" = "directory" ]; then
  BACKUP_FILE_EXT=""
  OUTPUT="--out"
fi

# shellcheck disable=SC2034
BACKUP_FILE_NAME="$BACKUP_DATE-$MONGO_DB$BACKUP_FILE_EXT"

echo "Backing up database $MONGO_DB with user $MONGO_USER on host $MONGO_HOST:$MONGO_PORT. Saving on $(backup_path)"
mongodump --host=$MONGO_HOST --port=$MONGO_PORT --username=$MONGO_USER --password=$MONGO_PASSWORD --db=$MONGO_DB --authenticationDatabase=$MONGO_AUTH_DB $OUTPUT="$(backup_path)"

on_backup_success
